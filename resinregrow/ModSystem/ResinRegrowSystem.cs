namespace resinregrow.ModSystem
{
    using HarmonyLib;
    using Vintagestory.API.Common;
    using Vintagestory.API.Server;

    public class ResinRegrowSystem : ModSystem
    {
        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return true;
        }

        public override void StartServerSide(ICoreServerAPI api)
        {
            // Apply patches with harmony
            var harmony = new Harmony(this.Mod.Info.ModID);
            harmony.PatchAll();
            base.Start(api);
        }
    }
}
